# NOTE as Wed May 4 20:14:34 CST 2016, this file is depreciated. I meant to have
# a script to generate git commit as patches but in a second thought, it might
# be as useful as I intend. Not 100% sure.

DIST = dist

.PHONY: all patches clean
all: patches
	@echo ">>> Showing Some Status <<<"
	@git status

GS_PATCH = dist/gs-sle-classic-ext.patch
GSE_PATCH = dist/gse-sle-classic-ext.patch
patches:
	@echo "* Creating GS patch..."
	@git diff sp2-3.20 master -- sp2-rebasing/gnome-shell-3.20.1/ > ${GS_PATCH}
	@sed -i 's|sp2-rebasing/gnome-shell-3.20.1/||' ${GS_PATCH}
	@echo "* Creating GSE patch..."
	@git diff sp2-3.20 master -- sp2-rebasing/gnome-shell-extensions-3.20.0/ > ${GSE_PATCH}
	@sed -i 's|sp2-rebasing/gnome-shell-extensions-3.20.0/||' ${GSE_PATCH}

clean:
	rm -rvf ${DIST}/*
