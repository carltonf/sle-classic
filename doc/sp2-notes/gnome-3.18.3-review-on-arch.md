**Authoring Time**: Dec-29-2015 11:38:09 CST

# Report on 3.18.3

1.  Install 'gnome-3.18.3' on Arch (gnome and gnome-extra groups). All packages
    in group `gnome` and `gnome-extra` has been installed.
2.  Use 'bundled' `gnome-classic` for usage test.
3.  Reviews are separated in 2 parts: usage check and code review, the latter is
    further separated into two parts: git log check and source check. Source
    check is **NOT** done yet.
4.  Legends: `[Y]` for good points, `[N]` for bad points and `[X]` for uncertain
    ones. As I mainly set out looking for problems, expect more bad points
    anyway. However, for changes that might improve the overall development,
    I'll mark as uncertain despite the fact that they break current code.

## Main summary (opinionated)

-   Extension part feels like being rewritten completely. Shell, Mutter APIs have
    also undergone some changes

    **Opinion:** This is the part concerns `SlE-classic` most, it'll be like working on a
    completely new code base.

-   The `HiDPI` support is a main change/feature across the whole `GNOME`. Most UI
    have been changed to accommodate scaled texts and reactive size.

    **Opinion:** `HiDPI` code would require extra effort to understand. This is
    also a source of potential bugs. It should not affect multi monitor
    scenario, but it's also unlikely a feature we can turn off (we might not
    allow configuration though).

    **PS:** From what I know, it's a wired-in feature to support (super) high
    resolution screen, which becomes popular recently on mobile devices.

-   There are many UI changes in newer GNOME Shell and bundled applications.

    **Opinion:** It would not be practical to maintain backward UI compatibility.

## Usage check

-   [Y] `Alt-F2` quick command is still available

    `r` and `lg` (and `rt` to reload resource only). Though looking glass's
    color is weird.

-   [Y] The overall experience is much smoother.

-   [N] the notification indicator is removed from bottom panel.

    Actually, there is no indicator once a popup message fades. (not even in
    Overview, users have to manually open the calendar).

-   [N] no easy way to add app shortcut on desktop.

    In current sle-classic, David added this feature as part of the context menu
    for application menu entries (also implemented by David, no context menu in
    upstream for `app-menu` yet).

-   [N] No `Desktop` quick entry in Nautilus

    Inconsistency: if we manually enable "icons on desktop" in tweak tool, the
    Desktop entry will be added. However icons are already on desktop in classic
    mode.

-   [X] Only raise windows on the same workspace when activating them

    commit `086fcd4a32ffff`

    Windows not in the same workspace will only trigger a notification instead of
    getting switched to as in SLE-12.

### `gdm`

-   [N] The behavior to leave a gdm console open is kept as the current version

    In my test, switching user frequently leads to freeze of the whole computer,
    nothing short of cable unplug can restore the machine.

    **Maybe there is something I should configure before using gdm in Arch**

### `tweak-tool`

(**OPININ:**) My limited tests have shown that just as in current version,
`tweak-tool` is still unstable, inconsistent and there is no help documentation
bundled, not ready for "non-techie" end user to use.

-   [X] Many more customization options

    For example, there is now an option to disable animation.

    **Font Scale**: there is also an option to scale font size for `HiDPI`, which is
    nice, but scaling can potentially cause other UI problems.

-   [N] dynamic/static workspace is still buggy

    Assuming caused by the same issue fixed in sle-classic already but there are
    some problems not observed in the original bug.

    If there are more desktops than the static configuration, switching back from
    dynamic **OR** decrease the number of desktops even in static would crash the
    shell. Two crashes in a line kill the shell in "Oh no" message.

-   [N] Just as in current version, the extensions status in tweak tool doesn't
    reflect their real status in classic mode.

-   [N] window grouping is still buggy

    A bug fixed in current sle-classic. The desktop seems to be still managed by
    Nautilus as there is a Nautilus `Files`'s window button once "grouping" is
    enabled (supposedly the one that manages the desktop).

### Extra stuff

1.  `Wayland`

    According to [Wayland - ArchWiki](https://wiki.archlinux.org/index.php/Wayland): it(`Wayland`) is an indirect dependency of gtk2
    and gtk3.

    **Q**: Can we avoid this dependency or simply include Wayland in SP2?

2.  World Clocks / Time zones

    There is a world clock list in Calendar, so it's improved greatly as suggested
    by Frederic in bnc#862416.

## Git log check

**SKIM THROUGH** commits for potential break/important change. `This is a quick
skim-through`.

[2015-12-29 Tue] I skim from the current **HEAD** to `3.10.*`

-   [X] Shell Theme have been adapted to SASS format from CSS.

    Requires a bit more time to understand as SASS introduces some features not in
    CSS. Generally a good thing, as SASS is more manageable than vanilla CSS
    (almost an industry standard in web development anyway).

    A small separate repo is created as `gnome-shell-sass` for common styles.

    commit `e317bfdab7abd27a285b6`: introduce `gnome-shell-sass` as a submodule in
    `gnome-shell`, but the decision to use `SASS` can be dated to `0ded0dbfd5ea`.

-   [X] General impression: Most changes that are related to
    Network(NetworkManager) are before 3.14

### `gnome-shell`

Many more changes than extensions, at first glance I do not know how they would
affect `sle-classic`. Only list what I know in the below:

-   [X] new infrastructure `GTask`

    from commit `bf0be6ef12` to `1a39666f7c6`, main parts of the shell are being
    moved to `GTask`. Do not know more to comment.

-   [N] "rt" in runDialog to reload GResource

    commit `0b9e68e3056a0`

    reload resource? maybe it's about bundled shell files. I don't want "reload
    resource" but "live development"!

-   [N] Previous asset files(css/img) and js sources are bundled into a `GResource`
      file formats.

    JS are loaded from GResource from commit `1ebb162a00bf6c4b0d714` following
    changes in `gjs`.

    Theme files are converted to GResource format in commit from
    `328bb1c21ba56b` to `49c4ba5656b13c055`.


    This is a change around 3.14 and this change creates some extra steps in
    development. I remembered an article detailed steps how to unbundle `GResource`
    and get back the old live style shell development. **Not sure it's still
    applicable**

-   [X] a series of shell code style cleaning/refactoring

    from commit `9dd3162dbea523d6` to commit `84eda6e45927`

    Mark for reference.

-   [N] The commit that removes message indicator

    commit `77413feb573baa0` as part of [a series of patches](https://bugzilla.gnome.org/show_bug.cgi?id%3D744850) that integrate
    notification into calendar.

-   [X] Startup changes for ibus

    commit `6a36a68f3289c3160c`. relevant to ibus people.

-   [X] Indicator class change

    commit `a0a701757ef47e`. Indicator class is the base class for all
    indicators but not for message tray indicator and workspace indicator in
    sle-classic (due to limitations of Indicator class itself, not flexible
    enough).

-   [X] Background system rewrite

    commit `650dea017b551b`. This is a big rewrite after Mutter has changed
    first. Currently, there are multiple issues related to background (some
    fixed, some persists, some only get workarounds).

-   [X] changes related to texture-cache

    commit `6f00d81abf426e17b7ccbb` and `4184edc7f8cd17e0db`. Related to icons
    and `cogl`, which I lack enough understanding but it has been the center of
    discussions in multiple visual bugs.

-   [X] js adapt to GSettings API change

    commit `83cb26d70eac7b`.

-   [Y] session and session schema overrides

    commit `ae2751a68b20ab281c`. There are session-specific overrides for each
    session. This commit claims that "Now GSettings is expected to grow support
    for session specific defaults". However, so far I have not found any
    improvement while resolving a recent bug related to `workspace`
    (`bnc#951346`) which is caused to the hard-coded "session-mode =>
    override-schema relation".

    Watch this out in newer versions.

-   [X] window menus get implemented in gnome-shell

    commit `e7af257814b1b56a4`. Not sure about the impact.

-   [N] texture-cache and `HiDPI`

    commit `9ecf466ce110c0` and commit `9c88fec4fce`.

-   [Y] a UI group for modal dialogs

    commit `2974b29f159` and `1b78dd662b12c9fa`. All modal dialogs share the
    same parent -- the modal ui group.

    This might help fix some bugs related to simultaneous modal dialogs (e.g.
    there was one about using encrypted usb disk, the dialog for root privilege
    and disk password can potentially overlap each other, yielding a useless
    state).

-   [X] Various fixes related to background.

    from commit `ec6facb9e` to `55d1c7e2ab9`. Should not be relevant as
    background has been written in later version. Mark here for possible
    references.

-   [X] the earliest hiDPI part?

    commit `d868e6bfaff4e11742`.

-   [X] app-system

    from commit `027c3d166153`, multiple changes related to app-system.
    `app-system` is relevant when dealing with window lists.

-   [X] `StTextureCache`: adapt to MetaWindow changing icon prop type

    commit `af889168f0fe572b3`. it seems that icon cache is changed to use
    Cairo. There used to some bugs related to icons/texture, though I can't
    remember any problems caused by the underlying mechanism.

-   [N] theme icon management change and high contrast themes (accessibility)

    commit from `4eb0a672db4126bbe166f` to `f4cc3327e8694764`. Only SVGs are
    supported. Possibly affect Yast. There is also a fate about icons in
    sle-classic theme.

-   [X] St: css margin property

    commit `2935fd4ffe9176d28f68`. needed when we need to tweak the theme. Pay
    attention to the fact: "In the case that a CSS margin is not specified, we
    don't to set a value of 0 to the clutter actor margin. In this manner it
    allows to use Clutter margin values set in the code. However, the margins
    that are set both in the code and in the CSS on the same side, the result is
    unpredictable."

-   [Y] N_() globally

    commit `facaea6850eabeac8d`. interesting commit to show how to add a shell
    global functions.

-   [Y] gnome shell works with python 3

    commit `be3c3c64c164e1b`. A small compatible library
    [Python Six](http://pythonhosted.org/six/) is used.

-   review ends at commit `02f2f694e4c1a5`
    3.10.1


### `gnome-shell-extensions`

[2015-12-29 Tue] Overall, < 10 commits from 3.18.3 to current 3.19.x, among
which no significant changes are found.

-   [N] `window-list`

    commits `61ec98f504f1caae7e` and `b97c111bde8b363ae04`

    `window-list` is changed to reactive style, i.e. its height and width will
    adapt to the font size.

-   [N] `app-menu`'s signal handling is revamped.

    commit `487c089e862f5b`

-   [N] [Bug 745064 – GTK+3 applications Icons not shown on the taskbar in the Wayland session](https://bugzilla.gnome.org/show_bug.cgi?id=745064)

    commit `c0170ff9456df798fea`.

    NOTE: Window grouping used to have bugs related to titles.

-   [N] The code that removes tray button

    commit `04ec4de375251` removes the tray button. This is a breaking change in
    UI, we might not be able to ignore.

-   [X] The startup/build sequence of classic session is changed.

    commit `43c4e7fa2ec1c1ba4`

    `sle-classic` is closely modeled after `classic` in both build, startup
    sequence. I think depending on the change in `gnome-shell`, this might make
    adding a new session like `sle-classic` can be harder or easier. Hard to say
    here yet.

    commit `e08d527d51e5c3`

    this commit introduces environment variable that define the gnome session type
    to launch.

-   [N] `HiDPI` theme variant

    commit `5ba4e68f175cb866`. It seems that the need to support `HiDPI` requires
    changes on theme. This might affect `sle-theme` and possibly expand the
    original fate on new SVG icon to new set of theme.

-   [X] The new Extension object

    commit `5fc66444b6afce0f`.

-   [X] multiple commits have shown there is some breaking changes in Mutter API

    Supposedly extensions and shell should talk to `ShellGlobal` objects but the
    abstraction is not really complete, so I had to deal with Mutter API directly
    for some bugs.

-   [X] mouse scroll on window button (and workspace indicator) switches workspace

    introduced in commit `a42dcee6ecae73dba`. A nice feature to have.

    Mark here as this might require some extra care when integrating window-list
    into main panel as in sle-classic.

-   [X] API change: `skip_taskbar` replaces `Shell.WindowTracker.is_window_interesting()`

    commit `68adf77012e5d`. The old API is used in one patch dealing with window
    grouping bug.

-   Review ends at commit `b62a65e1a8b`.

## Source check

**NOTE** No time for now.

# About 3.19+

3.18 to 3.20 is only one major version bump, 3.19 (as all odd numbered versions)
are just testing releases. I'll **assume** it's trivial to find out what has
changed from 3.18 in 3.20 by reading the source code.

-   `jhbuild` would take too long to setup

    In retrospect, the instability would be a more acute issue in testing release
    anyway.

-   There are no GNOME dev packages available neither in official channel nor AUR

    A discussion ref: [Gnome 3.19 in Arch](https://bbs.archlinux.org/viewtopic.php?id=206139).
