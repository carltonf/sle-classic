// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Lang = imports.lang;
const Clutter = imports.gi.Clutter;
const St = imports.gi.St;

const PanelMenu = imports.ui.panelMenu;

const AboutMenuButton = new Lang.Class({
    Name: 'AboutMenuButton',
    Extends: PanelMenu.Button,
    _init: function() {
        this._hostname = null;
        this._updateHostnameId = 0;
        this._ticket = 1;

        let hbox;
        let vbox;
        let menuAlignment = 0.25;

        if (Clutter.get_default_text_direction() == Clutter.TextDirection.RTL)
            menuAlignment = 1.0 - menuAlignment;
        this.parent(menuAlignment, 'About Me');

        this.about_hbox = new St.BoxLayout({ style_class: 'panel-status-menu-box' });
        this.hostname_label = new St.Label({y_align: Clutter.ActorAlign.CENTER});
        this.about_hbox.add_child(this.hostname_label);

        this.actor.add_child(this.about_hbox);
        hbox = new St.BoxLayout({ name: 'aboutArea' });
        this.menu.box.add_child(hbox);

        vbox = new St.BoxLayout({vertical: true});
        hbox.add(vbox);

        this._os_release = Gio.File.new_for_path('/etc/os-release');
        let success, contents, tag;
        try {
            [success, contents, tag] = this._os_release.load_contents(null);
        } catch (e) {
            contents = 'SUSE Linux Enterprise';
        }
        let match = new RegExp('(.+)PRETTY_NAME=(.+)ID(.+)').exec(contents.toString().replace(/\n/g, ' '));
        let sysinfo_text;
        if (!match) {
            sysinfo_text = 'SUSE Linux Enterprise';
        } else {
            sysinfo_text = match[2].toString().replace(/"/g, ' ');
        }

        this._sysinfo = new St.Label({ text: sysinfo_text,
                                     can_focus: true });
        vbox.add(this._sysinfo);
        this.actor.hide();

        this._updateHostnameId = GLib.timeout_add(GLib.PRIORITY_DEFAULT,
                                                  this._ticket,
                                                  Lang.bind(this, function() {
                                                      if (this._ticket < 60*60)
                                                          this._ticket *= 2;
                                                      this._updateHostnameId = 0;
                                                      this._updateHostname();
                                                      return false;
                                                  }));

        return;
    },

    _updateHostname: function(){
        let command = 'hostname';
        let hostname_text;
        try {
            let [res, stdout, stderr, status] = GLib.spawn_command_line_sync(command);
            hostname_text = String.fromCharCode.apply(null, stdout);
        } catch (e) {
            hostname_text = 'localhost';
        }

        if ((this._hostname == null) || (this._hostname != hostname_text)) {
            this._ticket = 1;
            this._hostname = hostname_text;
            this.hostname_label.set_text(this._hostname);
            this.actor.show();
        }
        this._updateHostnameId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT,
                                                  this._ticket,
                                                  Lang.bind(this, function() {
                                                      if (this._ticket < 60*60)
                                                          this._ticket *= 2;
                                                      this._updateHostnameId = 0;
                                                      this._updateHostname();
                                                      return false;
                                                  }));
    },

    _destroy: function() {
        this._ticket = 1;
        if (this._updateHostnameId) {
            GLib.source_remove (this._updateHostnameId);
            this._updateHostnameId = 0;
        }
    },

});
