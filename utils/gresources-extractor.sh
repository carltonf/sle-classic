#!/bin/sh

## Original source
# https://blogs.gnome.org/mclasen/2014/03/24/keeping-gnome-shell-approachable/

### Working Directory
mkdir -pv /opt/gnome-shell-js
cd /opt/gnome-shell-js

### Core
echo "* Unpacking GNOME core JS files"

gs=/usr/lib64/gnome-shell/libgnome-shell.so

mkdir -p ui/components ui/status misc perf extensionPrefs gdm portalHelper

for r in `gresource list $gs`; do
    gresource extract $gs $r > ${r/#\/org\/gnome\/shell/.}
done

### JS Extensions
# JS extension files are stored in plain files as before. Here a link is made
# only for convenience
# NOTE: the relative path is used to enable hacking from remote machines with
# sshfs (mount the root)
ln -svfn ../../usr/share/gnome-shell/extensions/ .

### Env & GDM 
echo 'GNOME_SHELL_JS=/opt/gnome-shell-js' >> /etc/environment
echo -e "** WARNING: GNOME_SHELL_JS is set in /etc/environment for convenience.\n"      \
  "* gnome-shell process needs this variable to find your unpacked sources. \n"         \
  "* Other options: for GDM you can add environment variable in systemd service file.\n"\
  "* IMPORTANT: unset this variable if you want test newly installed GNOME Shell.\n"    \
  "(or run this script again to unpack the new ones here)."
